
// SnifferDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include"pcap.h"
#include "Protocol.h"
#include "utilities.h"

// CSnifferDlg 对话框
class CSnifferDlg : public CDialogEx
{
// 构造
public:
	CSnifferDlg(CWnd* pParent = NULL);	// 标准构造函数

										// 对话框数据

#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SNIFFER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

public://///////////////////////////////////////////[my fuction]//////////////////////////////////////////////
	int lixsniff_initCap();
	int lixsniff_startCap();
	int lixsniff_updateTree(int index);
	int lixsniff_updateEdit(int index);
	int lixsniff_updateNPacket();
	int lixsniff_saveFile();
	int lixsniff_readFile(CString path);
	//////////////////////////////////////////////［my data］/////////////////////////////////////////////
	int devCount;
	struct pktcount npacket;				//各类数据包计数
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_if_t *alldev;
	pcap_if_t *dev;
	pcap_t *adhandle;
	pcap_dumper_t *dumpfile;
	char filepath[512];							//	文件保存路径
	char filename[64];							//	文件名称							

	HANDLE m_ThreadHandle;			//线程

	CPtrList m_pktList;							//捕获包所存放的链表
	CPtrList m_localDataList;				//保存被本地化后的数据包
	CPtrList m_netDataList;					//保存从网络中直接获取的数据包
	CBitmapButton m_bitButton;		//图片按钮
	int npkt;
	int listchoice;
	// 实现

protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CButton m_buttonStop;
	CButton m_buttonRead;
	CButton m_buttonSave;
//	CButton m_buttonStar;
	CComboBox m_comboBox;
	CComboBox m_comboBoxRule;
	CEdit m_edit;
	CListCtrl m_listCtrl;
	CTreeCtrl m_treeCtrl;
	CEdit m_editNUdp;
	CEdit m_editNTcp;
	CEdit m_editNOther;
	CEdit m_editNSum;
	CEdit m_editNHttp;
	CEdit m_editNIpv4;
	CEdit m_editNIpv6;
	CEdit m_editNIcmp;
	CEdit m_editNDns;
	CEdit m_editNFtp;
	CEdit m_editIcmpv6;
	afx_msg void OnItemchangedList2(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCustomdrawList2(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonStar();
	afx_msg void OnBnClickedButtonEnd();
	afx_msg void OnBnClickedButtonSave();
	CButton m_buttonStart;
};
